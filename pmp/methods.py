import os
import requests, re
import pypandoc as ppandoc
import frontmatter
from unidecode import unidecode
import datetime


def fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, filename):
    mdblop = requests.get(urlmd)
    mdblop = mdblop.text

    # extraction metadata vs content
    metadata, content = frontmatter.parse(mdblop, encoding='utf-8', handler=None)
    
    theme = 'static/pink.css' # default css
    layout = 'static/notes.css' #default layout
    template = 'templates/notes.html' #default layout
    
    # extraction title
    if not(metadata.get('title')):
        title = re.search(r'^#\s(.*)', content, re.M|re.I)
        if (title):
            vtitle = '--metadata=title:' + title.group(0)[2:]
        else:
            vtitle = '--metadata=title:no title'
    else:
        vtitle = '--metadata=title:' + metadata.get('title')
    
    # extraction theme
    if(metadata.get('theme')):
        print('using theme:', metadata.get('theme'))
        theme = 'static/' + metadata.get('theme') + '.css'
        vcss = '--css=/' + theme
    else:
        vcss = '--css=/' + theme

    # extraction layout
    if(metadata.get('layout')):
        print('using layout:', metadata.get('layout'))
        layout = 'static/' + metadata.get('layout') + '.css'
        vlayout = '--css=/' + layout
    else:
        vlayout = '--css=/' + layout

    # only one template
    vtemplate = '--template=' + template

       # extraction tags & categorised tags
    html_tags = make_tags(content) # produit une liste html des tags simples
    html_cattags = make_cattags(content) # produit une liste html des tags catégorisés
    html_alltags = html_tags + html_cattags

    # function as pattern for re.compile/sub
    def tag_replace(match):
        tag = match.group(2)
        normtag = unidecode(match.group(2))
        return match.group(1) + '[[#' + tag + '](#tags-' + normtag + ')]{.tags-' + normtag+ '}' + match.group(3)

    def cattag_replace(match):
        cat = match.group(1)
        normcat = unidecode(cat)
        tag = match.group(2)
        normtag = unidecode(tag)
        return '[[#' + cat + ':' + tag + '](#' + normcat + '-' + normtag + ')]{.' + normcat + '-' + normtag + '}'

    re_cattags = re.compile(r'#(\w+):(\w+)')        # prepare la regex pour les tags catégorisés
    mdblop = re_cattags.sub(cattag_replace, mdblop) # remplace dans le texte

    re_tags = re.compile(r'(\s|,)#(\w+)(\s|,)') # prepare la regex pour trouver les tags
    mdblop = re_tags.sub(tag_replace, mdblop)   # remplace dans le texte


    if(metadata.get('bibliography')):
        print(metadata.get('bibliography'))
        bibfilename = filename + '.bib'
        with open(bibfilename, 'wb') as f:
            r = requests.get(metadata.get('bibliography'), stream=True)
            f.writelines(r.iter_content(1024))
    
        vbiblio = '--bibliography=' + filename + '.bib'

        # prepare pandoc arguments
        pdoc_args = ['--standalone', 
                    '--section-divs',
                    '--toc',
                    '--lua-filter=templates/date.lua',
                    '--citeproc', # added if bibliography
                    '--log=logpandoc.log',
                    vbiblio,
                    vservice,
                    vpadid,
                    vpathid,
                    vindex,
                    vtitle,
                    urlwrite,
                    vtemplate,
                    vlayout,
                    vcss
                    ]
    elif(metadata.get('references')):
        pdoc_args = ['--standalone', 
                    '--section-divs',
                    '--toc',
                    '--lua-filter=templates/date.lua',
                    '--citeproc', # added if references
                    '--log=logpandoc.log',
                    vservice,
                    vpadid,
                    vpathid,
                    vindex,
                    vtitle,
                    urlwrite,
                    vtemplate,
                    vlayout,
                    vcss
                    ]
    else:
        pdoc_args = ['--standalone', 
                    '--section-divs',
                    '--toc',
                    '--lua-filter=templates/date.lua',
                    '--log=logpandoc.log',
                    vservice,
                    vpadid,
                    vpathid,
                    vindex,
                    vtitle,
                    urlwrite,
                    vtemplate,
                    vlayout,
                    vcss
                    ]


    # convert md source
    output = ppandoc.convert_text(mdblop, 'html', format='markdown+hard_line_breaks', extra_args=pdoc_args, outputfile=filename)

    # open converted html
    with open(filename) as file:
        htmlpage = file.read()
        file.close()

    # include tags in html
    if (html_tags):
        htmlpage = htmlpage.replace("html-tags", html_tags)

    # include categorised tags in html
    if (html_cattags):
        htmlpage = htmlpage.replace("html-cattags", html_cattags)

    # get generic script for tags
    with open("templates/tags.html") as file:
        script = file.read()
        # script = re.sub(r'\$filekey\$', filekey, script)
        file.close()
    
    # include script in html
    htmlpage = htmlpage.replace("<script/>", script)
    
    # replace html file so that it can be served as static file
    with open(filename, 'w') as file:
        file.write(htmlpage)
        file.close()

    return htmlpage

# function preparing md list of tags converted in html
def make_tags(content):
    tags = re.findall(r'#\w+', content)  # trouve toutes les occurrences de tags
    cats = re.findall(r'#\w+:', content) # trouve toutes les occurrences de catégories de tags
    cats = [sub[: -1] for sub in cats]   # reprend cette liste et retire le : de chaque occurrence
    tags = list(set(tags) - set(cats))   # soustrait les deux listes pour que les catégories 
                                         # ne se retrouvent pas dans les tags
    tags.sort()

    md_tags = '**tags:**'
    for tag in set(tags):
        md_tags = md_tags + ' [[%s](#tags-%s)]{.tags-%s}' % (tag, unidecode(tag[1:]), unidecode(tag[1:])) # crée un item de list avec un span de classe

    return ppandoc.convert_text(md_tags, 'html', format='markdown')

# function preparing md list of categorised tags converted in html
def make_cattags(content):
    cats = re.findall(r'#\w+:', content) # trouve toutes les occurrences de catégories de tags
    cats = [sub[: -1] for sub in cats]   # reprend cette liste et retire le : de chaque occurrence
    md_cattags = ''
    for cat in set(cats):
        md_cattags = md_cattags + '\n\n**' + cat[1:] + '**:'
        cattags = re.findall(rf'{cat}:\w+', content)
        print(cattags)
        for cattag in set(cattags):
            category = re.split(r':', cattag)
            md_cattags = md_cattags + ' [[#%s](#%s-%s)]{.%s-%s}' % (category[1], unidecode(category[0][1:]), unidecode(category[1]), unidecode(category[0][1:]), unidecode(category[1])) # crée un item de list avec un span de classe

    return ppandoc.convert_text(md_cattags, 'html', format='markdown')

# function writing activity log
def activity_log(service, padid, mode):
    x = datetime.datetime.now()

    with open("activities.log", 'a') as logs:
        logs.write(f'\n{x.strftime("%c")} {service} {padid} : {mode}')
        logs.close()

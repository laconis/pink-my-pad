import sys
import os
from . import webapp

HELP_STR = "Usage: python3 -m pmp webapp"

if len(sys.argv) == 1 or len(sys.argv) > 2:
    print(HELP_STR)
    exit(1)

if sys.argv[1] == "feeder":
    # interval = float(os.environ.get("FR24_FEEDER_INTERVAL", 3))  # defaults to 3 minutes
    # feeder.main(interval)
    raise NotImplementedError("Not implemented yet")
elif sys.argv[1] == "webapp":
    webapp.app.run(host="0.0.0.0")
elif sys.argv[1] == "reset-db":
    raise NotImplementedError("Not implemented yet")
else:
    print(HELP_STR)
    exit(1)

import os
import requests, re
from flask import Flask, url_for, request
import pypandoc as ppandoc
import mimetypes
from .methods import fetch_and_convert, activity_log
from markupsafe import escape


srcdir = os.path.dirname(__file__)

app = Flask(__name__)
app.debug = True

def fetch_one_url(url):
    # data = "data"
    # escaped = re.sub(r'\W+',"_",url)
    # filename = os.path.join(data, escaped)
    if os.path.exists(filename):
        with open(filename, 'r') as file:
            return file.read()
    else:
        if not os.path.exists(data):
            os.makedirs(data)
        request = requests.get(url)
        with open(filename, 'a') as file:
            file.write(request.text)
            return request.text

@app.route("/", methods=['GET', 'POST'])
def route_index():
    # hedgedoc pad id for index
    ## index = "8HlnFAX5SOemdfkC657W_w" (sur hedgedoc)
    index = "RGs4PRHjQaODpAGymwNiQw" # (sur libreon)
    # primary variables
    service = "libreon"
    baseurl = "pad.libreon.fr/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + index + requete
    
    # making paths
    pservice = './'
    padfilename = 'index.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + index
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:True'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + index + write
    
    if request.method == 'POST':
        print("post index → refresh")
        activity_log(service, "index", "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get index → exist → file read")
                activity_log(service, "index", "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get index → not exist → fetch")
            activity_log(service, "index", "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/about/')
def projects():
    return '<h1>À propos</h1><p>Pink my pad est développé par Nicolas Sauret.</p>'

@app.route('/hedgedoc/<padid>', methods=['GET', 'POST'])
def hedgedoc(padid):
    print('Pad Hedgedoc : ', padid)
    # primary variables
    service = "hedgedoc"
    baseurl = "demo.hedgedoc.org/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/libreon/<padid>', methods=['GET', 'POST'])
def libreon(padid):
    print('Pad Libreon : ', padid)
    # primary variables
    service = "libreon"
    baseurl = "pad.libreon.fr/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/hackmd/<padid>', methods=['GET', 'POST'])
def hackmd(padid):
    print('Pad hackmd.io : ', padid)
    # primary variables
    service = "hackmd"
    baseurl = "hackmd.io/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/lescommuns/<padid>', methods=['GET', 'POST'])
def lescommuns(padid):
    print('Pad lescommuns.org : ', padid)
    # primary variables
    service = "lescommuns"
    baseurl = "pad.lescommuns.org/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/lamyne/<padid>', methods=['GET', 'POST'])
def lamyne(padid):
    print('Pad lamyne.org : ', padid)
    # primary variables
    service = "lamyne"
    baseurl = "pad.lamyne.org/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)

@app.route('/numerique-en-commun/<padid>', methods=['GET', 'POST'])
def numeriqueencommun(padid):
    print('Pad numerique-en-commun.fr : ', padid)
    # primary variables
    service = "numerique-en-commun"
    baseurl = "pad.numerique-en-commun.fr/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)


@app.route('/codimd.math.cnrs/<padid>', methods=['GET', 'POST'])
def mathcnrs(padid):
    print('Pad codimd.math.cnrs.fr : ', padid)
    # primary variables
    service = "codimd.math.cnrs"
    baseurl = "codimd.math.cnrs.fr/"
    protocole = "https://"
    requete = "/download"
    write = '?edit'

    # making urls
    urlmd = protocole + baseurl + padid + requete
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:'
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + baseurl + padid + write
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)


# @app.route('/framapad/<path:subpath>')
# def show_subpath(subpath):
#     # show the subpath after /path/
#     return f'Subpath {escape(subpath)}'

@app.route('/framapad/<path:subpath>', methods=['GET', 'POST'])
def framapad(subpath):
    print('Pad framapad : ', subpath)
    # primary variables
    pathid = subpath
    padid = pathid.split('/')[2]
    service = "framapad"
    protocole = "https://"
    requete = "/export/markdown"

    # making urls
    urlmd = protocole + pathid + requete
    print(urlmd)
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:' + pathid
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + pathid 
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)


@app.route('/ensad/<path:subpath>', methods=['GET', 'POST'])
def ensad(subpath):
    print('EtherPad Ensad : ', subpath)
    # primary variables
    pathid = subpath
    padid = pathid.split('/')[2]
    service = "ensad"
    protocole = "https://"
    requete = "/export/markdown"

    # making urls
    urlmd = protocole + pathid + requete
    print(urlmd)
    
    # making paths
    pservice = os.path.join('./', service)
    padfilename = padid + '.html'
    pathfilename = os.path.join(pservice, padfilename)
    
    # making pandoc variables & parameters
    vservice = '--variable=vservice:' + service
    vpadid = '--variable=vpadid:' + padid
    vpathid = '--variable=vpathid:' + pathid
    vindex = '--variable=vindex:'
    urlwrite = '--variable=urlwrite:' + protocole + pathid 
    # css = url_for('static', filename='notes.css')
    # vcss = '--css='+css
    
    if request.method == 'POST':
        print("post → refresh")
        activity_log(service, padid, "refresh")
        return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)
    else:
        if os.path.exists(pathfilename):
            with open(pathfilename, 'r') as file:
                print("get → exist → file read")
                activity_log(service, padid, "read")
                return file.read()
        else:
            # if not os.path.exists(data):
            #     os.makedirs(data)
            print("get → not exist → fetch")
            activity_log(service, padid, "create")
            return fetch_and_convert(urlmd, vservice, vpadid, vpathid, vindex, urlwrite, pathfilename)


# @app.get("/static/notes.css")
# def static_notescss():
#     filename = os.path.join('static', 'notes.css')
#     # mimetypes.add_type("text/css", ".css", True)
#     print('Guess from .css : ',mimetypes.guess_type('/static/notes.css'))
#     print('Read mimetypes : ',mimetypes.read_mime_types('/static/notes.css'))
#     with open(filename) as fp:
#         staticfile = fp.read()
#         return staticfile, 200, {'Content-Type': 'text/css'}

@app.get("/static/<filename>")
def route_static(filename):
    filename = os.path.join('static', filename)
    # mimetypes.add_type("text/css", ".css", True)
    # print('Guess from filename : ',mimetypes.guess_type(filename))
    # print('Read mimetypes : ',mimetypes.read_mime_types(filename))
    with open(filename) as fp:
        staticfile = fp.read()
        if mimetypes.guess_type(filename)[0] == 'text/css':
            return staticfile, 200, {'Content-Type': 'text/css'}
        elif mimetypes.guess_type(filename)[0] == 'text/javascript':
            return staticfile, 200, {'Content-Type': 'text/javascript'}
        else:
            return staticfile

@app.get("/keys/<filename>")
def route_keys(filename):
    filename = os.path.join('keys', filename)
    with open(filename) as fp:
        keyfile = fp.read()
        return keyfile

@app.route("/welcome/")
def welcome():
 return "Welcome to my webpage!" 

# with app.test_request_context():
#     print(url_for('index'))
#     print(url_for('projects', next='/'))
#     print(url_for('hedgedoc', padid='X0X0X0X0X'))



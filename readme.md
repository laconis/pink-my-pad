# Pink My Pad

[Pink my pad](https://pinkmypad.net) is a simple application that turns and serves your online pads into static HTML pages.

What is it for ? It provides a straightforward publishing solution for your pad, making it more stable, customizable at will, and open to discussion.

Pink my pad is made with [Flask](https://flask.palletsprojects.com/) and [Pandoc](https://pandoc.org/), and includes [Hypothes.is](https://web.hypothes.is) for annotation.

## Usage

Create, update, customize and share your own pages from your pads. 

Visit [pinkmypad.net](https://pinkmypad.net) to start using the application. It is fully free and open. 


## Install locally 

### Preresquisite

- `python3` 
- `python3-pip` 
- `python3-venv` 
- `pandoc 3.*` (unless you install `pypandoc_binary` which includes Pandoc, as recommended for production)

### Installation (in a Python virtual env)

```bash
# Create a python venv
python3 -m venv venv
# activate the env
source venv/bin/activate
# install dependencies
pip install -r requirements.txt
```

## Initiate necessary files and directories

```bash
mkdir hedgedoc hackmd lescommuns lamyne framapad
echo "# Activity log" > activities.log
```

## Launch

```bash
python3 -m pmp webapp
```

## Install for production (nginx)

Nothing specific for production. 

I suggest to follow this Digital Ocean's installation tutorial: 

- [serving flask applications with gunicorn and nginx](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-20-04)

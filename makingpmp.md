# making pmp

Quelques réflexions en faisant..

- [x] refresh
- [x] refresh index
- [ ] template
    - [x] gestion d'une css externe
    - [ ] choix css via yaml
- [x] schema d'adresse
    - [x] hedgedoc/hackmd
    - [x] framapad
    - [ ] etherpad
- [ ] mots-clés catégorisés

## Mots-clés catégorisés

- Regex sur le markdown pour lister les mots-clés (liste markdown)
- création d'un module html (pandoc liste-md → liste-html)
- substitute une variable dans le template par le module html 

## refresh

je dois trouver un trigger entre la page et une commande python.
Par défaut, l'application charge ce qui a déjà été transformé (fetch_one_url), mais on peut tester et comparer la date ?

J'ai trouvé le trigger (simple button qui relance la page), mais on doit pouvoir faire mieux : 
1. charger le html quand on demande une url
2. forcer le refresh quand on ajoute un paramètre ( par exemple `?refresh`)

ou différencier get et post ! voir https://flask.palletsprojects.com/en/2.2.x/quickstart/#http-methods

En différenciant le get et le post, on peut n'avoir un refresh que si le bouton refresh est appuyé (POST). Sinon, lorsqu'on fait un simple GET, l'application sert le html existant par défaut, et fait une requête seulement si nécessaire.

## Schéma d'adresses

proposition de construire les adresses en suivant le schéma : `/service/idpad`.

exemple: 
- `/hedgedoc/XOXOXOXOX/`
- `/etherpad/XOXOXOXOX/`
- `/hackmd/XOXOXOX/`

comment traiter les anciens ? je propose d'homogénéiser les schémas d'adresse, et d'utiliser canonical pour récupérer les annotations.



## Choix de template:

principe : une donnée yaml définit le template qui sera utilisé: 

```
template: [notes, cards, site, ]
```

## Déploiement


Config flask
Ressources apache : 
- most useful : https://www.rosehosting.com/blog/how-to-install-flask-on-ubuntu-22-04-with-apache-and-wsgi/
- similar : https://tecadmin.net/deploying-flask-application-on-ubuntu-apache-wsgi/
- alternative method: https://stackoverflow.com/questions/71294518/deploying-flask-application-with-apache2-proxy-server
- official Virtual Host documentation : https://httpd.apache.org/docs/2.4/vhosts/examples.html